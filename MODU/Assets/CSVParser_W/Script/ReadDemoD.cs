using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public struct SEquipmentData{

	public string	_id;
	public string 	_name;
	public string _type01;
	public string _type02;
	public string _type03;
	public string _type04;
	public string _type05;
	public string _type06;
	public string _type07;
	public string _type08;
	public string _type09;
	public string _type10;

};

public struct DictionaryData{

	public int _keyValue;
	public SEquipmentData _equipmentData;
}

public class ReadDemoD : MonoBehaviour {
	
	static public Dictionary<int, SEquipmentData> CSVEquipmentData01 = new Dictionary<int , SEquipmentData>();
	
	private string urlMODU = "";
	
	public bool activeName01 = false;

	public TextAsset localTextMODU;

	public string csvFile = null;

	private WWW wwwCSVFile;
	private WWW downCSVFile;

	public bool wwwChk = false;
	public bool downloadChk = false;
	public bool localChk = false;

	//public string text = "";

	//class CSVReaderD;

	void Awake()
	{
		localTextMODU = Resources.Load("MODUData") as TextAsset;

		print (Application.persistentDataPath);
	}

	IEnumerator Start() {

		urlMODU = "https://docs.google.com/spreadsheets/d/1q2Gczb9JT20LOwDKNke3gCWp3cz84eqmrAWBJjDBxfM/pub?gid=1085698593&single=true&output=csv";
		//urlMODU = "https://docs.google.com/spreadsheets/d/1q2Gczb9JT20LOwDKNke3gCWp3cz84eqmrAWBJjDBxfM/pub?gid=1166826186&single=true&output=csv";

		DontDestroyOnLoad(transform.gameObject);

		if(Application.internetReachability == NetworkReachability.NotReachable)
		{
			print ("network disconnect");
		}

		else if(Application.internetReachability != NetworkReachability.NotReachable)
		{
			print ("network connect");
		}

		/*
		WWWForm form = new WWWForm();
		form.AddField("Cache-Control", "no-cache");
		
		//wwwCSVFile = new WWW(urlMODU, form.data, form.headers);
		wwwCSVFile = new WWW(urlMODU, form);

		//Caching.CleanCache();
		*/

		//MODU
		wwwCSVFile = new WWW (urlMODU);
		yield return wwwCSVFile;

		//if(wwwCSVFile.size > 0)
		if(wwwCSVFile.error == null && wwwCSVFile.size > 0 && (Application.internetReachability != NetworkReachability.NotReachable))
		{
			print (wwwCSVFile.text);
			wwwChk = true;
			//text = string.Format("{0}, wwwCSVFile", wwwCSVFile.size);
			print("MODU - wifi parsing success");

			csvFile = Application.persistentDataPath + "/fileName.txt";

			//if(System.IO.File.Exists(csvFile))
			FileStream fileStream = new FileStream(csvFile, FileMode.Create);
			StreamWriter streamWriter = new StreamWriter(fileStream);

			streamWriter.Write(wwwCSVFile.text);
			streamWriter.Flush();
			streamWriter.Close();

			print ("Save Path = " + csvFile);

			CSVReaderD.dataInitSUserData (CSVReaderD.SplitCsvGrid(wwwCSVFile.text), CSVEquipmentData01);

			wwwCSVFile.Dispose ();

			//CSVReaderD.dataInitSUserData (CSVReaderD.SplitCsvGrid(downCSVFile.text), CSVEquipmentData01);
		}
		
		else
		{
			/*
			csvFile = Application.persistentDataPath + "/fileName.txt";

			downCSVFile = new WWW("file:///" + csvFile);
			yield return downCSVFile;
			text = string.Format("{0}, downCSVFile", downCSVFile.size);


			FileStream fileStream = new FileStream(csvFile, FileMode.Open, FileAccess.Read);
			StreamReader streamReader = new StreamReader(fileStrem);


			downloadChk = true;
			print("MODU - downloaded parsing success");
			CSVReaderD.dataInitSUserData (CSVReaderD.SplitCsvGrid (downCSVFile.text), CSVEquipmentData01);

			downCSVFile.Dispose();
			*/

			csvFile = Application.persistentDataPath + "/fileName.txt";
			downCSVFile = new WWW("file:///" + csvFile);
			yield return downCSVFile;

			if(downCSVFile.error == null && downCSVFile.size > 0)
			{
				//text = string.Format("{0}, downCSVFile", downCSVFile.size);
				downloadChk = true;
				print("MODU - downloaded parsing success");
				CSVReaderD.dataInitSUserData (CSVReaderD.SplitCsvGrid (downCSVFile.text), CSVEquipmentData01);
			}

			else
			{
				localChk = true;
				print("MODU - local parsing success");
				CSVReaderD.dataInitSUserData (CSVReaderD.SplitCsvGrid (localTextMODU.text), CSVEquipmentData01);
			}

			downCSVFile.Dispose();
		}
	}

	public bool activeNameMODU
	{
		set{
			this.activeName01 = value;
		}
		get{
			return activeName01;
		}
	}
}

