﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq; 


public class CSVReaderD : MonoBehaviour 
{
	
	static int worldCount = 0;
	
	public TextAsset csvFile; 
	static public Dictionary<int, SEquipmentData> CSVEquipmentData = new Dictionary<int , SEquipmentData>();
	
	//public Dictionary<int, SUserData> equitmentData = new Dictionary<int, SUserData>();
	
	public void Start()
	{
		
		Debug.Log ("Init Local CSV File Loader Test");
		string[,] grid = SplitCsvGrid(csvFile.text);
		Debug.Log("size = " + (1+ grid.GetUpperBound(0)) + "," + (1 + grid.GetUpperBound(1))); 
		
		DebugOutputGrid(grid);
		
	}
	
	// outputs the content of a 2D array, useful for checking the importer
	static public void DebugOutputGrid(string[,] grid)
	{
		//임시로 
		string textOutput = ""; 
		//y number zero table value pass
		for (int y = 1; y < grid.GetUpperBound(1); y++) {	
			
			for (int x = 0; x < grid.GetUpperBound(0); x++) {
				
				textOutput += grid[x,y]; 
				textOutput += "|"; 
			}
			textOutput += "\n"; 
		}
		//Output String Value
		Debug.Log (textOutput);
		
	}
	
	static public void dataInitSUserData(string[,] grid, Dictionary<int, SEquipmentData> _csvEquipmentData)
	{
		
		SEquipmentData inputTemp = new SEquipmentData ();
		
		for (int y = 1; y < grid.GetUpperBound(1); y++) {	
			
			inputTemp._name					=(grid[1,y]);
			inputTemp._type01 				=(grid[2,y]);
			inputTemp._type02 				=(grid[3,y]);
			inputTemp._type03				=(grid[4,y]);
			inputTemp._type04			 	=(grid[5,y]);
			inputTemp._type05			 	=(grid[6,y]);
			
			_csvEquipmentData.Add (System.Int32.Parse (grid[0,y].ToString()), inputTemp);
		}
	}
	
	static public SEquipmentData getLoadSUserData(int index)
	{
		return CSVEquipmentData [index];
	}
	
	// splits a CSV file into a 2D string array
	//1
	static public string[,] SplitCsvGrid(string csvText)
	{
		print (" csvText SplitCsvFrid " + csvText);
		
		string[] lines = csvText.Split("\n"[0]); 
		
		// finds the max width of row
		int width = 0; 
		for (int i = 0; i < lines.Length; i++)
		{
			string[] row = SplitCsvLine( lines[i] ); 
			width = Mathf.Max(width, row.Length); 
		}
		Debug.Log ("width line : " + width + " / " + lines.Length);
		// creates new 2D string grid to output to
		string[,] outputGrid = new string[width + 1, lines.Length + 1];
		
		for (int y = 0; y < lines.Length; y++)
		{
			string[] row = SplitCsvLine( lines[y] ); 
			for (int x = 0; x < row.Length; x++) 
			{
				outputGrid[x,y] = row[x]; 
				
				// This line was to replace "" with " in my output. 
				// Include or edit it as you wish.
				outputGrid[x,y] = outputGrid[x,y].Replace("\"\"", "\"");
			}
		}
		return outputGrid; 
	}
	
	// splits a CSV row 
	static public string[] SplitCsvLine(string line)
	{
		return (from System.Text.RegularExpressions
		        .Match m in System.Text.RegularExpressions
		        .Regex.Matches(line,@"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)",System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
		        select m.Groups[1].Value).ToArray();
	}
}