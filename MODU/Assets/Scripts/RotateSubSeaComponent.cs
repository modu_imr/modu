using UnityEngine;
using System.Collections;

public class RotateSubSeaComponent : MonoBehaviour
{
	private bool		touch_background_;	
	private float		touch_position_x_;
	private float		touch_position_y_;
	private float		current_roll_angle_limit_;
	private float		current_yaw_angle_;
	
	// Use this for initialization
	void Start ()
	{
		current_roll_angle_limit_	= 0;
		current_yaw_angle_			= 0;
		touch_background_			= false;		
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (1 == Input.touchCount)
		{
			if (Input.GetTouch(0).phase == TouchPhase.Began)
			{
				touch_background_ = true;
				touch_position_x_ = Input.GetTouch(0).position.x;
				touch_position_y_ = Input.GetTouch(0).position.y;
			}
			else if (Input.GetTouch(0).phase == TouchPhase.Ended)
			{
				touch_background_ = false;
			}
			
			if (touch_background_)
			{
				Vector2 touch_position = Input.GetTouch(0).position;
				
				current_roll_angle_limit_	+= (touch_position.y - touch_position_y_);
				current_roll_angle_limit_	= Mathf.Clamp(current_roll_angle_limit_, -30, 30);
				current_yaw_angle_			-= touch_position.x - touch_position_x_;
				this.transform.rotation = Quaternion.Euler(current_roll_angle_limit_, current_yaw_angle_, 0);
				
				if (touch_position_y_ != touch_position.y)
				{
					touch_position_y_ = touch_position.y;
				}			
				
				if (touch_position_x_ != touch_position.x)
				{	
					touch_position_x_ = touch_position.x;
				}
			}
		}
	}
}
