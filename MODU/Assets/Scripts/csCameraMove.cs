using UnityEngine;
using System.Collections;

public class csCameraMove : MonoBehaviour {
	private float distance_ = 0.0f;
	private float current_distance_ = 0.0f;
	private const float CHANGE_DISTANCE = 5.0f;
	
	private bool touch_background_ = false;
	private float touch_position_x_ = 0.0f;
	private float touch_position_y_ = 0.0f;
	private const float MAX_UD_ROTATION_ANGLE = 20.0f;
	
	private const float ROTATION_CONST = 0.3f;
	private const float MOVE_CONST = 1.2f;
	
	private bool multi_touch_background_ = false;
	private float multiTouchPos_y = 0.0f;
	private const float MAX_Y_POSITION = 10.0f;
	private const float MIN_Y_POSITION = 3.0f;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {		
		move();
		rotateUDLR();
	}
	
	void move() {
		if (1 < Input.touchCount)
		{
			Vector3 pos = transform.position;
			
			// multi touch moved up == move Up
			/*
			if (Input.GetTouch(0).phase == TouchPhase.Began)
			{
				multi_touch_background_ = true;
				multiTouchPos_y = Input.GetTouch(0).position.y;
			}
			else if ((Input.GetTouch(0).phase == TouchPhase.Ended) ||
					 (Input.GetTouch(1).phase == TouchPhase.Ended) )
			{
				multi_touch_background_ = false;
			}
			*/
			
			//if (multi_touch_background_)
			//{
				moveFB(ref pos);
				//moveUD(ref pos);
			//}
			
			transform.position = pos;
		}
		else
		{
			distance_			= 0.0f;
			current_distance_	= 0.0f;
		}
	}
	
	void rotateUDLR() {
		/*
		if (Input.GetMouseButtonDown(0))
		{
				touch_background_ = true;
				touch_position_x_ = Input.GetTouch(0).position.x;
				touch_position_y_ = Input.GetTouch(0).position.y;
		}
			else if (Input.GetMouseButtonUp(0))
			{
				touch_background_ = false;
			}
			
			if (touch_background_)
			{
				Vector3 angle = transform.localEulerAngles;
				Vector2 touchPos = Input.mousePosition;
				
				angle.y += (touchPos.x - touch_position_x_);
				angle.x -= (touchPos.y - touch_position_y_);
			
			
			if((angle.x > MAX_UD_ROTATION_ANGLE) && (angle.x < (360.0f-MAX_UD_ROTATION_ANGLE))) {
				if(angle.x < 180.0f) {
					angle.x = MAX_UD_ROTATION_ANGLE;
				}
				else if(angle.x >= 180.0f) {
					angle.x = 360.0f - MAX_UD_ROTATION_ANGLE;
				}
			}
			
				
			//Debug.Log(angle.x);
			
				transform.localRotation = Quaternion.Euler(angle);
				
				if (touch_position_y_ != touchPos.y)
				{
					touch_position_y_ = touchPos.y;
				}			
				
				if (touch_position_x_ != touchPos.x)
				{	
					touch_position_x_ = touchPos.x;
				}
			}
		*/
		
		
		if(Input.touches.Length == 0) {
			return;
		}
		
		//if (1 == Input.touchCount)
		if(2 == Input.touches[0].tapCount)
		{
			if (Input.GetTouch(0).phase == TouchPhase.Began)
			{
				touch_background_ = true;
				touch_position_x_ = Input.GetTouch(0).position.x;
				touch_position_y_ = Input.GetTouch(0).position.y;
			}
			else if (Input.GetTouch(0).phase == TouchPhase.Ended)
			{
				touch_background_ = false;
			}
			
			if (touch_background_)
			{
				Vector3 angle = transform.localEulerAngles;
				Vector2 touchPos = Input.GetTouch(0).position;
				
				angle.y += (touchPos.x - touch_position_x_) * ROTATION_CONST;
				angle.x -= (touchPos.y - touch_position_y_) * ROTATION_CONST;
				
				if((angle.x > MAX_UD_ROTATION_ANGLE) && (angle.x < (360.0f-MAX_UD_ROTATION_ANGLE))) {
					if(angle.x < 180.0f) {
						angle.x = MAX_UD_ROTATION_ANGLE;
					}
					else if(angle.x >= 180.0f) {
						angle.x = 360.0f - MAX_UD_ROTATION_ANGLE;
					}
				}
				
				transform.localRotation = Quaternion.Euler(angle);
				
				if (touch_position_y_ != touchPos.y)
				{
					touch_position_y_ = touchPos.y;
				}			
				
				if (touch_position_x_ != touchPos.x)
				{	
					touch_position_x_ = touchPos.x;
				}
			}
		}
	}
	
	void moveFB(ref Vector3 _pos) {
		current_distance_ = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
		
		if (0.0f == distance_) {
			distance_ = current_distance_;
		}
		
		if (distance_ != current_distance_) {
			Vector3 forwardVec = transform.forward;
			float temp = (current_distance_ - distance_) * Time.deltaTime;	
				
			if(temp > 0) {
				_pos += forwardVec * MOVE_CONST;
			}
			else {
				_pos += forwardVec * MOVE_CONST * (-1.0f);
			}
			
			_pos.z = Mathf.Clamp(_pos.z, -75, 75);
			_pos.x = Mathf.Clamp(_pos.x, -75, 75);
			//
			_pos.y = 5.0f;
				
			distance_ = current_distance_;
		}
	}
	
	/*
	void moveUD(ref Vector3 _pos) {
		Vector2 touchPos = Input.GetTouch(0).position;
				
		_pos.y += (touchPos.y - multiTouchPos_y) * MOVE_CONST;
		_pos.y = Mathf.Clamp(_pos.y, MIN_Y_POSITION, MAX_Y_POSITION);
		
		if (multiTouchPos_y != touchPos.y) {
			multiTouchPos_y = touchPos.y;
		}
	}
	*/
}
