﻿using UnityEngine;
using System.Collections;

public class WController_detailText : MonoBehaviour {
	public GameObject _rotateObject = null;

	public float rotationSpeed = 3.0f;
	public float lerpSpeed = 1.0f;
	
	private Vector3 speed = new Vector3();
	private Vector3 avgSpeed = new Vector3();
	private bool dragging = false;

	public float max_Y = 430;
	public float min_Y = 275;
	
	void OnMouseDrag() {
		print ("this name : " + transform.name);
		dragging = true;
	}

	void OnMouseExit()
	{
		dragging = false;
	}
	
	// Update is called once per frame
	void Update () {

		//keyUpdate ();
		touchUpdate ();

	}

	void keyUpdate()
	{
		if (Input.GetKey (KeyCode.A)) {
			_rotateObject.transform.position += new Vector3(0.0f, .1f, 0.0f);
		}
	}

	void touchUpdate()
	{
		
		
		
		//if (Input.GetMouseButton(0) && dragging) {
		if(1 == Input.touchCount && dragging){
			//speed = new Vector3(0, Input.GetAxis("Mouse Y"), 0);
			speed = new Vector3(0, Input.GetTouch(0).deltaPosition.y, 0);
			avgSpeed = Vector3.Lerp (avgSpeed, speed, Time.deltaTime * 2);
		}
		else {
			if (dragging) {
				speed = avgSpeed;
				dragging = false;
				
			}
			
			float i = Time.deltaTime * lerpSpeed;
			speed = Vector3.Lerp(speed, Vector3.zero, i);
		}
		
		
		//print ("speed.x : " + speed.y);
		
		if (speed.y > 0) {
			if(_rotateObject.transform.localPosition.y < max_Y)
			{
				print ("+speed : "+speed.y + "// position y :  "+_rotateObject.transform.localPosition.y);
				_rotateObject.transform.Translate (Camera.main.transform.up * speed.y * rotationSpeed, Space.World);
			}
			else
			{
				_rotateObject.transform.localPosition = new Vector3(transform.localPosition.x, max_Y, transform.localPosition.z);
			}
			
		} else if(speed.y < 0){
			if(_rotateObject.transform.localPosition.y > min_Y)
			{
				print ("-speed : "+speed.y + "// position y :  "+_rotateObject.transform.localPosition.y);
				_rotateObject.transform.Translate (Camera.main.transform.up * speed.y * rotationSpeed, Space.World);
			}
			else
			{
				_rotateObject.transform.localPosition = new Vector3(transform.localPosition.x, min_Y, transform.localPosition.z);
				
			}
		}
		
		//_rotateObject.transform.position = new Vector3(720, Mathf.Clamp (_rotateObject.transform.position.y, -430, 430), -1000);
	}

}
