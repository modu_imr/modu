using UnityEngine;
using System.Collections;

public class ObjectSceneToClick : MonoBehaviour {
	private ObjectNameManager objectNameScript;
	
	void Start() {
		GameObject objName = GameObject.Find("ObjectNameManager");
		
		if(objName != null) {
			objectNameScript = objName.GetComponent<ObjectNameManager>();
		}
	}
	
	void OnClick() {
		if(objectNameScript != null)
		{
			Application.LoadLevel("Scene_" + objectNameScript.objectName);
		}
	}
}