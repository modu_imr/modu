using UnityEngine;
using System.Collections;

public class ZoomInOut : MonoBehaviour
{

	public GameObject targetObject = null;
	private GameObject scaleObject = null;
	private GameObject rotateObject = null;

	private float distance_;
	private float current_distance_;
	private bool dragging = false;
	// Use this for initialization


	private bool		touch_background_;	
	private float		touch_position_x_;
	private float		touch_position_y_;
	private float		current_roll_angle_limit_;
	private float		current_yaw_angle_;

	public float		maxScale_X = 3.5f;
	public float		minScale_X = 0.8f;

	public float        maxAngle_X = 330;
	public float		minAngle_X = 210;

	public float        maxSpeed;
	public float		temp = 0.0f;

	void Start ()
	{
		rotateObject = scaleObject = targetObject;

		distance_			= 0.0f;
		current_distance_	= 0.0f;

		current_roll_angle_limit_	= 270;
		current_yaw_angle_			= 0;
		touch_background_			= false;	

	}

	void OnMouseDrag()
	{
		dragging = true;
		//print ("On Drag name : " + transform.name);
	}

	void OnMouseExit()
	{
		dragging = false;
	}

	// Update is called once per frame
	void Update ()
	{
		rotateUpdate ();
		zoomInOutUpdate ();
		testRotate ();


	}

	void zoomInOutUpdate()
	{
		if (2 == Input.touchCount)
		{
			//print ("transform Scale" + scaleObject.transform.localScale);

			current_distance_ = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
			if (0.0f == distance_)
			{
				distance_ = current_distance_;
			}

			if (distance_ != current_distance_)
			{
				temp = (current_distance_ - distance_) * Time.deltaTime;
				//this.transform.localScale *= (temp > 0 ? 1.2f : 0.8f);
				if(temp > 0)
				{
					Debug.Log("aaa");
					//if(1.8f < scaleObject.transform.localScale.x)
					if(maxScale_X < scaleObject.transform.localScale.x)
					{
						
					}
					else
					{
						Debug.Log("ccc");
						scaleObject.transform.localScale *= (1.05f * maxSpeed * 0.2f);
						//scaleObject.transform.localScale *= 1.05f;
					}
				}
				else
				{
					Debug.Log("bbb");
					//if(.8f > scaleObject.transform.localScale.x)
					if(minScale_X > scaleObject.transform.localScale.x)
					{
						
					}
					else
					{
						Debug.Log("ddd");
						scaleObject.transform.localScale *= (0.95f / (maxSpeed * 0.2f));
						//scaleObject.transform.localScale *= 0.95f;
						
					}
				}
				
				distance_ = current_distance_;
			}
		}
		
		else
		{
			distance_			= 0.0f;
			current_distance_	= 0.0f;
		}
	}

	void rotateUpdate()
	{
		if (1 == Input.touchCount) {

			if(TouchPhase.Moved == Input.GetTouch(0).phase && dragging)
			{

				Vector2 touch_position = Input.GetTouch (0).deltaPosition;
								
				current_roll_angle_limit_ += Input.GetTouch(0).deltaPosition.y * 0.3f;
				current_roll_angle_limit_ = Mathf.Clamp (current_roll_angle_limit_, minAngle_X, maxAngle_X);
				current_yaw_angle_ -= Input.GetTouch(0).deltaPosition.x * 0.3f;
				rotateObject.transform.rotation = Quaternion.Euler (current_roll_angle_limit_, current_yaw_angle_, 0);


			}
			else
			{
				dragging = false;
			}
		}

//		if (1 == Input.touchCount) {
//			if (Input.GetTouch (0).phase == TouchPhase.Began) {
//				touch_background_ = true;
//				touch_position_x_ = Input.GetTouch (0).deltaPosition.x;
//				touch_position_y_ = Input.GetTouch (0).deltaPosition.y;
//			} else if (Input.GetTouch (0).phase == TouchPhase.Ended) {
//				touch_background_ = false;
//				touch_position_x_ = Input.GetTouch (0).deltaPosition.x;
//				touch_position_y_ = Input.GetTouch (0).deltaPosition.y;
//			}
//			
//			if (touch_background_ && dragging) {
//
//				Vector2 touch_position = Input.GetTouch (0).deltaPosition;
//				
//				current_roll_angle_limit_ += (touch_position.y - touch_position_y_) * 0.3f;
//				current_roll_angle_limit_ = Mathf.Clamp (current_roll_angle_limit_, 240, 300);
//				current_yaw_angle_ -= (touch_position.x - touch_position_x_) * 0.3f;
//				rotateObject.transform.rotation = Quaternion.Euler (current_roll_angle_limit_, current_yaw_angle_, 0);
//				
//				if (touch_position_y_ != touch_position.y) {
//					//touch_position_y_ = touch_position.y;
//				}			
//				
//				if (touch_position_x_ != touch_position.x) {	
//					//touch_position_x_ = touch_position.x;
//				}
//			} else {
//				touch_position_x_ = Input.GetTouch (0).deltaPosition.x;
//				touch_position_y_ = Input.GetTouch (0).deltaPosition.y;
//			}
//		} else {
//			touch_position_x_ = 0.0f;
//			touch_position_y_ = 0.0f;
//		}
	}

	void testRotate()
	{
		if (Input.GetKey (KeyCode.Space))
		rotateObject.transform.Rotate (1, 0, 0, Space.World);
	}
}
