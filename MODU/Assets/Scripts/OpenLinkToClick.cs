﻿using UnityEngine;
using System.Collections;

public class OpenLinkToClick : MonoBehaviour {

	public string sURL;

	void OnClick()
	{
		Application.OpenURL(sURL);
	}
}