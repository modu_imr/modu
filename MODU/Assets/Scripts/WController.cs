﻿using UnityEngine;
using System.Collections;

public class WController : MonoBehaviour {

	public GameObject _rotateObject = null;

	public float rotationSpeed = 10.0f;
	public float lerpSpeed = 1.0f;
	
	private Vector3 speed = new Vector3();
	private Vector3 avgSpeed = new Vector3();
	private bool dragging = false;
	
	void OnMouseDrag() {

		print ("this name : " + transform.name);
		
		dragging = true;
	}

	void OnMouseExit()
	{
		dragging = false;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
		
		
		
		if (Input.GetMouseButton(0) && dragging) {
			speed = new Vector3(-Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
			avgSpeed = Vector3.Lerp (avgSpeed, speed, Time.deltaTime * 2);
		}
		else {
			if (dragging) {
				speed = avgSpeed;
				dragging = false;
			}
			
			float i = Time.deltaTime * lerpSpeed;
			speed = Vector3.Lerp(speed, Vector3.zero, i);
		}

		_rotateObject.transform.Rotate (Camera.main.transform.up * speed.x * rotationSpeed, Space.World);
		_rotateObject.transform.Rotate (Camera.main.transform.right * speed.y * rotationSpeed, Space.World);
		
	}
}
