using UnityEngine;
using System.Collections;

public class VideoHandheld : MonoBehaviour
{	
	public string videoFileName = "";
	
	// Use this for initialization
	void Start ()
	{
		Handheld.PlayFullScreenMovie(videoFileName, Color.clear, FullScreenMovieControlMode.Full);
		Application.LoadLevel("Main");
	}
}
