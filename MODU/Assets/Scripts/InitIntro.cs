﻿using UnityEngine;
using System.Collections;

public class InitIntro : MonoBehaviour {

	public GameObject equipmentDatabaseObject = null;

	private GameObject baseDataBase = null;
	
	// Use this for initialization
	void Start()
	{

		baseDataBase = GameObject.Find ("equipmentDatabase");

		if (null == baseDataBase) {
			//Debug.Log("Create baseDataBase");
			GameObject temp = Instantiate (equipmentDatabaseObject) as GameObject;
			temp.name = "equipmentDatabase";

		} else {
			//Debug.Log("already equipmentDatabaseObject Object Create");
		}
	}
}

