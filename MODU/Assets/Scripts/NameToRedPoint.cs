using UnityEngine;
using System.Collections;

public class NameToRedPoint : MonoBehaviour {

	public string strObject = "";

	public GameObject childObj;

	public bool blinkColor = false;

	public int count = 0;
	public int blinkSpeed = 0;
	
	private ObjectNameManager objectNameScript;
	
	void Start() {
		GameObject objName = GameObject.Find("ObjectNameManager");
		
		if(objName != null) {
			objectNameScript = objName.GetComponent<ObjectNameManager>();
		}

		//childObj = this.GetComponentInChildren<GameObject>();
	}
	
	void Update() {
	}
	
	void OnClick() {
		if(objectNameScript != null) {
			objectNameScript.objectName = strObject;

			//Application.LoadLevel("Scene_" + objectNameScript.objectName);
			StartCoroutine("NextSceneLoad");
		}		
	}

	private IEnumerator NextSceneLoad()
	{
		AsyncOperation async = Application.LoadLevelAsync("Scene_" + objectNameScript.objectName);

		while (async.isDone == false)
		{
			count++;

			if(blinkColor == false)
			{
				childObj.GetComponent<UISlicedSprite>().color = Color.black;
				blinkColor = true;
			}

			else if(blinkColor == true && count % blinkSpeed == 0)
			{
				childObj.GetComponent<UISlicedSprite>().color = Color.red;
				blinkColor = false;
			}

			yield return true;
		}
	}
}
