using UnityEngine;
using System.Collections;

public class SelectedObjectEnable : MonoBehaviour {
	
	public GameObject explaneObject;
	
	private ObjectNameManager objectNameScript;
	
	void Start() {
		GameObject objName = GameObject.Find("ObjectNameManager");
		
		if(objName != null) {
			objectNameScript = objName.GetComponent<ObjectNameManager>();
		}
		
		if(objectNameScript != null) {
			EnableComObject();
			SelectExplainSprite();
			
			//objectNameScript.isDestroy = true;
		}
	}
	
	void EnableComObject() {
		GameObject obj = GameObject.Find("ComObject");
			
		if(obj != null) {
			Transform comObj = obj.transform.FindChild("Com_"+objectNameScript.objectName);
				
			if(comObj != null) {
				comObj.gameObject.SetActive(true);
			}
		}
	}
	
	void SelectExplainSprite() {
		if(explaneObject != null) {
			GameObject explaneObj = explaneObject.transform.FindChild(objectNameScript.objectName+"Sprite").gameObject;
			
			if(explaneObj != null) {
				explaneObj.SetActive(true);
			}
		}
	}
}
