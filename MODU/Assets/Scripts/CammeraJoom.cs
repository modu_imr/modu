﻿using UnityEngine;
using System.Collections;

public class CammeraJoom : MonoBehaviour
{
    public Camera Camera;
    public Transform Object;

    public float maxScale_X = 3.5f;
    public float minScale_X = 0.8f;
    public float maxSpeed;

    public float temp = 0.0f;

    private float current_distance_;
    private float distance_;
    private bool dragging = false;

    void OnMouseDrag()
    {
        dragging = true;
    }

    void OnMouseExit()
    {
        dragging = false;
    }

    void Update()
    {
        //if (1 == Input.touchCount)
        //{
        //    if (TouchPhase.Moved == Input.GetTouch(0).phase && dragging)
        //    {
        //        Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
        //        Object.Translate(0, -touchDeltaPosition.y * maxSpeed, 0);
                
        //        //bool plus = Input.GetTouch(0).deltaPosition.y - Input.GetTouch(0).position.y > 0;

        //        //if (Object.transform.localPosition.y > -800 * (1 - Camera.orthographicSize / maxScale_X) + 5 && plus)
        //        //{
        //        //    Object.transform.localPosition += Vector3.up * -Time.deltaTime * 10;
        //        //}

        //        //if (Object.transform.localPosition.y < 800 * (1 - Camera.orthographicSize / maxScale_X) - 5 && !plus)
        //        //{
        //        //    Object.transform.localPosition += Vector3.up * Time.deltaTime * 10;
        //        //}
        //    }
        //    else
        //    {
        //        dragging = false;
        //    }
        //}

        if (2 == Input.touchCount)
        {
            current_distance_ = Vector2.Distance(Input.GetTouch(0).position, Input.GetTouch(1).position);
            if (0.0f == distance_)
            {
                distance_ = current_distance_;
            }

            if (distance_ != current_distance_)
            {
                temp = (current_distance_ - distance_) * Time.deltaTime;

                if (temp > 0)
                {
                    if (maxScale_X > Camera.orthographicSize)
                    {
                        Camera.orthographicSize += Time.deltaTime * maxSpeed;
                    }
                }
                else
                {
                    if (minScale_X < Camera.orthographicSize)
                    {
                        Camera.orthographicSize -= Time.deltaTime * maxSpeed;
                    }
                }

                distance_ = current_distance_;
            }
        }
        else
        {
            distance_ = 0.0f;
            current_distance_ = 0.0f;
        }
    }
}
