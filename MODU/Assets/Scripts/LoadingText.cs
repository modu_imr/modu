﻿using UnityEngine;
using System.Collections;

public class LoadingText : MonoBehaviour {

	public string loadText;

	void OnGUI()
	{
		int w = Screen.width, h = Screen.height;
		
		GUIStyle style = new GUIStyle();
		
		Rect rect = new Rect(w / 2, 0, w, h * 2 / 50);
		style.alignment = TextAnchor.UpperLeft;
		style.fontSize = h * 2 / 50;
		style.normal.textColor = new Color (1.0f, 1.0f, 1.0f, 1.0f);

		GUI.Label(rect, loadText, style);
	}
}
