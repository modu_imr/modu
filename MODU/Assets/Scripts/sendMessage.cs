﻿using UnityEngine;
using System.Collections;

public enum MBUTTONSTATE
{
	IDLE,
	FPSO,
	SUBSEA,
	MODU,
	RETURN,
	BACK
}

public class sendMessage : MonoBehaviour {
	
	public MBUTTONSTATE _mButtonState = MBUTTONSTATE.FPSO;
	public string returnSceneName = "";
	//public float touchSensertive = 0.1f;
	
	private GameObject mainButtonObject = null;
	
	private Vector3 startPos = Vector3.zero;
	private Vector3 mouseSpeed = Vector3.zero;
	private Vector3 touchSpeed = Vector3.zero;
	private Vector3 tempSpeed = Vector3.zero;
	private Vector3 speed = Vector3.zero;
	private float distance = 0.0f;
	
	private bool clickArea = false;
	private bool mouseInputOnce = false;
	
	// Use this for initialization
	void Start () {
		
		mainButtonObject = GameObject.Find ("Main_Button");
		if (null == mainButtonObject) {
			//Debug.Log("define not mainButtonObject");
		}
		
		startPos = transform.position;
	}
	
	void OnClick()
	{
		//Debug.Log ("return");
		
		switch (_mButtonState) {
		case MBUTTONSTATE.FPSO:
			mainButtonObject.SendMessage("FPSOButtonClick");
			break;
		case MBUTTONSTATE.SUBSEA:
			mainButtonObject.SendMessage("SUBSEAButtonClick");
			break;
		case MBUTTONSTATE.MODU:
			mainButtonObject.SendMessage("MODUButtonClick");
			break;
		case MBUTTONSTATE.RETURN:
			//mainButtonObject.SendMessage("RETURNButtonClick");	
			break;
		}
	}
	
	void OnMouseDown()
	{
		//Debug.Log ("OnMouseDown SendButton");
		clickArea = true;
	}
	
	void Update()
	{
		//	if (MBUTTONSTATE.RETURN != _mButtonState || MBUTTONSTATE.IDLE != _mButtonState ) {
		//		return;
		//}
		if (MBUTTONSTATE.RETURN == _mButtonState || MBUTTONSTATE.BACK == _mButtonState) {
			//mouseInput ();
			touchInput ();
		}
	}
	
	void mouseInput()
	{
		
		//Debug.Log("mouse Update");
		
		if (Input.GetMouseButtonDown (0)) {
			
			if(260 < Input.mousePosition.x && 310 > Input.mousePosition.x &&
			   60 > Input.mousePosition.y && 10 < Input.mousePosition.y)
			{
				clickArea = true;
				//Debug.Log("mouse 1");
			}
			
		}
		
		//Debug.Log ("mouse Pos : " + Input.mousePosition);
		if (Input.GetMouseButton (0) && clickArea)
		{
			//Debug.Log("mouse 3");
			//speed = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
			speed = new Vector3 (Input.GetAxis ("Mouse X"), 0, 0);
			
			
			if (-790 < transform.localPosition.x)
			{
			}
			else
			{
				speed = Vector3.Normalize(speed) * 0.01f;
				transform.position += speed;
			}
			
			if (-860 < transform.localPosition.x && false == mouseInputOnce) {
				
				mouseInputOnce = true;
				
				switch (_mButtonState) {	
				case MBUTTONSTATE.IDLE:
					print("Button State IDLE");
					break;
				case MBUTTONSTATE.FPSO:
					mainButtonObject.SendMessage("FPSOButtonClick");
					break;
				case MBUTTONSTATE.SUBSEA:
					mainButtonObject.SendMessage("SUBSEAButtonClick");
					break;
				case MBUTTONSTATE.MODU:
					mainButtonObject.SendMessage("MODUButtonClick");
					break;
				case MBUTTONSTATE.RETURN:
					mainButtonObject.SendMessage ("RETURNButtonClick");
					break;
				case MBUTTONSTATE.BACK:
					Application.LoadLevel (returnSceneName);
					break;
					
				}
				
			}
		}
		
		if (Input.GetMouseButtonUp (0)) {
			
			clickArea = false;
			mouseInputOnce = false;
			speed = Vector3.zero;
			transform.position = startPos;
			
		}

	}
	
	private void touchInput()
	{
		//Debug.Log("mouse Update");
		
		if (true == Input.GetMouseButtonDown (0) && 1 == Input.touchCount) {
			
			if(260 < Input.GetTouch (0).position.x && 310 > Input.GetTouch (0).position.x &&
			   60 > Input.GetTouch (0).position.y && 10 < Input.GetTouch (0).position.y)
			{
				clickArea = true;
				//Debug.Log("Touch 1");
			}
			
		}
		
		//Debug.Log ("mouse Pos : " + Input.mousePosition);
		if (1 == Input.touchCount && clickArea) {
			//Debug.Log ("mouse 3");
			//speed = new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0);
			speed = new Vector3 (Input.GetTouch (0).deltaPosition.x, 0, 0);
			
			
			if (-790 < transform.localPosition.x) {
			} else {
				speed = Vector3.Normalize (speed) * 0.02f;
				transform.position += speed;
			}
			
			if (-860 < transform.localPosition.x && false == mouseInputOnce) {

				mouseInputOnce = true;
				
				switch (_mButtonState) {	
				case MBUTTONSTATE.IDLE:
					print ("Button State IDLE");
					break;
				case MBUTTONSTATE.FPSO:
					mainButtonObject.SendMessage ("FPSOButtonClick");
					break;
				case MBUTTONSTATE.SUBSEA:
					mainButtonObject.SendMessage ("SUBSEAButtonClick");
					break;
				case MBUTTONSTATE.MODU:
					mainButtonObject.SendMessage ("MODUButtonClick");
					break;
				case MBUTTONSTATE.RETURN:
					mainButtonObject.SendMessage ("RETURNButtonClick");
					break;
				case MBUTTONSTATE.BACK:
					Application.LoadLevel (returnSceneName);
					break;
					
				}
				
			}
		} else {
			
			clickArea = false;
			mouseInputOnce = false;
			speed = Vector3.zero;
			transform.position = startPos;
			
		}
	}
}