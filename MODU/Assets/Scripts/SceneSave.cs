﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneSave : MonoBehaviour
{
    private static SceneSave instance;
    public  static SceneSave Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(SceneSave)) as SceneSave;
            }

            if (instance == null)
            {
                GameObject obj = new GameObject("YoutubeVideoPlayer_Mobile");
                instance = obj.AddComponent<SceneSave>() as SceneSave;
                instance.youtube = obj.AddComponent<YoutubeVideo>();
            }

            return instance;
        }
    }

    public YoutubeVideo youtube;

    public int LevelNumber { get; set; }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
}