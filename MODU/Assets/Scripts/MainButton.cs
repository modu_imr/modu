﻿using UnityEngine;
using System.Collections;

public class MainButton : MonoBehaviour {

	private GameObject FPSO_Button;
	private GameObject SUBSEA_Button;
	private GameObject MODU_Button;
	private GameObject FPSO_SubButton;
	private GameObject SUBSEA_SubButton;
	private GameObject MODU_SubButton;
	private GameObject RETURN_Button;

	private GameObject tFPSO;
	private GameObject tSUBSEA;
	private GameObject tMODU;
	private GameObject sFPSO;
	private GameObject sSUBSEA;
	private GameObject sMODU;

	private bool FPSO_Press = false;
	private bool SUBSEA_Press = false;
	private bool MODU_Press = false;



	// Use this for initialization
	void Start () {
	
		FPSO_Button = GameObject.Find ("FPSO");
		SUBSEA_Button = GameObject.Find ("SUBSEA");
		MODU_Button = GameObject.Find ("MODU");
		RETURN_Button = GameObject.Find ("MainmenuButton");
		FPSO_SubButton = GameObject.Find ("FPSO_SubButton");
		SUBSEA_SubButton = GameObject.Find ("SUBSEA_SubButton");
		MODU_SubButton = GameObject.Find ("MODU_SubButton");
		tFPSO = GameObject.Find ("tFPSO");
		tSUBSEA = GameObject.Find ("tSUBSEA");
		tMODU = GameObject.Find ("tMODU");
		sFPSO = GameObject.Find ("sFPSO");
		sSUBSEA = GameObject.Find ("sSUBSEA");
		sMODU = GameObject.Find ("sMODU");

		subButtonController (false, false, false);
		ButtonController (true, true, true, false);

	}

	void OnEnter()
	{
		ButtonController (true, true, true, false);
		subButtonController (false, false, false);
	}

	// Update is called once per frame
	void Update () {

	}
	
	public void RETURNButtonClick()
	{
		//Debug.Log ("RETURNButton Click");
		ButtonController (true, true, true, false);
		subButtonController (false, false, false);
	}

	public void FPSOButtonClick()
	{
		//Debug.Log ("FPSOButton Click");
		ButtonController (true, false, false, true);
		subButtonController (true, false, false);
	}

	public void SUBSEAButtonClick()
	{
		//Debug.Log ("SUBSEAButton Click");
		ButtonController (false, true, false, true);
		subButtonController (false, true, false);
	}

	public void MODUButtonClick()
	{
		//Debug.Log ("MODUButton Click");
		ButtonController (false, false, true, true);
		subButtonController (false, false, true);
	}

	void ButtonController(bool a, bool b, bool c, bool d)
	{
		FPSO_Button.SetActive (a);
		SUBSEA_Button.SetActive (b);
		MODU_Button.SetActive (c);
		RETURN_Button.SetActive (d);
		//Text Converting
		tFPSO.SetActive (a);
		tSUBSEA.SetActive (b);
		tMODU.SetActive (c);
	}

	void subButtonController(bool a, bool b, bool c)
	{
		//Debug.Log ("name : " + transform.name);

		FPSO_SubButton.SetActive (a);
		SUBSEA_SubButton.SetActive (b);
		MODU_SubButton.SetActive (c);
		//Text Converting
		sFPSO.SetActive (a);
		sSUBSEA.SetActive (b);
		sMODU.SetActive (c);

	}
	
}
