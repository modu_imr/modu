﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayVideo : MonoBehaviour
{
    [SerializeField]
    private string link;

    [SerializeField]
    private Text text;

    [SerializeField]
    GameObject playVideo, nowLoading;

    private float startTime;

    //void Update()
    //{
    //    if (startTime != 0 && startTime + 0.5f <= Time.time)
    //    {
    //        Play();
    //        startTime = 0;
    //    }
    //}

    void OnMouseDown()
    {
        //startTime = Time.time;
        //if (playVideo != null)
        //{
        //    playVideo.SetActive(false);
        //    nowLoading.SetActive(true);
        //}
        //text.text = "NowLoading";
        Play();
    }

    public void Play()
    {
        Debug.Log("Started Video - Only run on mobile");
        StartCoroutine(PlayYoutube(SceneSave.Instance.youtube.RequestVideo(link, 360)));
    }

    public IEnumerator PlayYoutube(string url)
    {
        yield return Handheld.PlayFullScreenMovie(url, Color.black, FullScreenMovieControlMode.Full, FullScreenMovieScalingMode.Fill);
        Debug.Log("below this line will run when the video is finished");
        VideoFinished();
    }

    private void VideoFinished()
    {
        //if (playVideo != null)
        //{
        //    playVideo.SetActive(true);
        //    nowLoading.SetActive(false);
        //}

        //text.text = "Play Video";
        //SceneManager.LoadScene(0);
    }
}